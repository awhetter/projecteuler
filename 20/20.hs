factorial :: Integer -> Integer
factorial 1 = 1
factorial toFactorial = toFactorial * (factorial (toFactorial - 1))
		
charToDigit :: Char -> Int
charToDigit toParse =
	case toParse of
		'0' -> 0
		'1' -> 1
		'2' -> 2
		'3' -> 3
		'4' -> 4
		'5' -> 5
		'6' -> 6
		'7' -> 7
		'8' -> 8
		'9' -> 9
		
sumOfString :: String -> Int
sumOfString "" = 0
sumOfString (digit:rest) = (charToDigit digit) + (sumOfString rest)

answer :: Int
answer = sumOfString $ show $ factorial 100