getSum :: Integer -> Integer
getSum 1 = 1
getSum toPower = (toPower ^ toPower) + (getSum (toPower - 1))

getFromEnd :: Int -> [a] -> [a]
getFromEnd theLength theList = reverse $ take theLength $ reverse theList

answer :: String
answer = getFromEnd 10 $ show $ getSum 1000