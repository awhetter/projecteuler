getFibs :: [Int]
getFibs = fibbing (1,1) 4000000
   where
      fibbing :: (Int,Int) -> Int -> [Int]
      fibbing (x,y) lim = if y < lim
         then [y] ++ fibbing (y,(x+y)) lim
         else []

getEvenFibs :: [Int] -> [Int]
getEvenFibs [] = []
getEvenFibs (x:xs) = if x `mod` 2 == 0
   then [x] ++ getEvenFibs xs
   else [] ++ getEvenFibs xs

answer :: Int
answer = sum $ getEvenFibs getFibs
