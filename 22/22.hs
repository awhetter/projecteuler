import System.IO
import Data.List (sort)

charToAlphabetPos :: Char -> Int
charToAlphabetPos toConvert =
	case toConvert of
		'A' -> 1
		'B' -> 2
		'C' -> 3
		'D' -> 4
		'E' -> 5
		'F' -> 6
		'G' -> 7
		'H' -> 8
		'I' -> 9
		'J' -> 10
		'K' -> 11
		'L' -> 12
		'M' -> 13
		'N' -> 14
		'O' -> 15
		'P' -> 16
		'Q' -> 17
		'R' -> 18
		'S' -> 19
		'T' -> 20
		'U' -> 21
		'V' -> 22
		'W' -> 23
		'X' -> 24
		'Y' -> 25
		'Z' -> 26
		_ -> error $ "Tried to convert non alphabetic charater to number!"

sumOfString :: String -> Int
sumOfString "" = 0
sumOfString (digit:rest) = (charToAlphabetPos digit) + (sumOfString rest)

readNextWord :: Handle -> IO ( Maybe String )
readNextWord fileToRead = do
	reachedEOF <- hIsEOF fileToRead
	if reachedEOF then
		return Nothing
	else do
		gotString <- actuallyGetWord fileToRead ""
		return $ Just gotString
	where
		actuallyGetWord :: Handle -> String -> IO ( String )
		actuallyGetWord fileToRead toAddTo = do
			gotChar <- hGetChar fileToRead
			if gotChar == '"' then do
				hSeek fileToRead RelativeSeek 2
				return toAddTo
			else
				actuallyGetWord fileToRead $ toAddTo ++ [gotChar]

getWords :: Handle -> [String] -> IO ( [String] )
getWords fileToRead wordsSoFar = do 
	wordToProcess <- readNextWord fileToRead
	case wordToProcess of
		Nothing -> do
			hClose fileToRead
			return wordsSoFar
		Just val ->
			getWords fileToRead $ wordsSoFar ++ [val]

genList :: (Num a) => [a] -> a -> [a]
genList [] _ = []
genList (x:xs) position = [(x * position)] ++ (genList xs (position + 1))
			
answer :: IO()
answer = do
	fileToRead <- openFile "names.txt" ReadMode
	hSeek fileToRead RelativeSeek 1
	allWords <- getWords fileToRead []
	putStrLn $ show $ sum $ genList (map sumOfString (sort allWords)) 1