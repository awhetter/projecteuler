import System.IO

isTriangleNumber :: (Integral a) => a -> Bool
isTriangleNumber toCheck = elem toCheck [(fst.properFraction) (0.5 * n * (n-1)) | n <- [1..25]]

charToAlphabetPos :: Char -> Int
charToAlphabetPos toConvert =
	case toConvert of
		'A' -> 1
		'B' -> 2
		'C' -> 3
		'D' -> 4
		'E' -> 5
		'F' -> 6
		'G' -> 7
		'H' -> 8
		'I' -> 9
		'J' -> 10
		'K' -> 11
		'L' -> 12
		'M' -> 13
		'N' -> 14
		'O' -> 15
		'P' -> 16
		'Q' -> 17
		'R' -> 18
		'S' -> 19
		'T' -> 20
		'U' -> 21
		'V' -> 22
		'W' -> 23
		'X' -> 24
		'Y' -> 25
		'Z' -> 26
		_ -> error $ "Tried to convert non alphabetic charater to number!"

sumOfString :: String -> Int
sumOfString "" = 0
sumOfString (digit:rest) = (charToAlphabetPos digit) + (sumOfString rest)
		
readNextWord :: Handle -> IO ( Maybe String )
readNextWord fileToRead = do
	reachedEOF <- hIsEOF fileToRead
	if reachedEOF then
		return Nothing
	else do
		gotString <- actuallyGetWord fileToRead ""
		return $ Just gotString
	where
		actuallyGetWord :: Handle -> String -> IO ( String )
		actuallyGetWord fileToRead toAddTo = do
			gotChar <- hGetChar fileToRead
			if gotChar == '"' then do
				hSeek fileToRead RelativeSeek 2
				return toAddTo
			else
				actuallyGetWord fileToRead $ toAddTo ++ [gotChar]

getWords :: Handle -> Int -> IO ( Int )
getWords fileToRead numOfTriangles = do 
	wordToProcess <- readNextWord fileToRead
	case wordToProcess of
		Nothing -> do
			hClose fileToRead
			return numOfTriangles
		Just val ->
			if isTriangleNumber (sumOfString val) then
				getWords fileToRead $ numOfTriangles + 1
			else
				getWords fileToRead numOfTriangles
				
answer :: IO()
answer = do
	fileToRead <- openFile "words.txt" ReadMode
	hSeek fileToRead RelativeSeek 1
	numOfTriangles <- getWords fileToRead 0
	putStrLn $ show numOfTriangles