dividesByEach :: Int -> [Int] -> Bool
dividesByEach _ [] = True
dividesByEach num (x:xs) = if num `mod` x == 0
   then dividesByEach num xs
   else False

smallestDBE :: Int -> Int
smallestDBE x = if dividesByEach x [2..20]
   then x
   else smallestDBE $ x+20

answer :: Int
answer = smallestDBE 40
