isPrime :: Int -> Bool
isPrime x = hiddenIsPrime x $ x-1
   where
      hiddenIsPrime :: Int -> Int -> Bool
      hiddenIsPrime x 1 = True
      hiddenIsPrime x y = if x `mod` y == 0
         then False
         else hiddenIsPrime x (y-1)

listOfFactors :: Int -> [Int]
listOfFactors x = [x] ++ (hiddenLOF x $ x `div` 2)
   where
      hiddenLOF :: Int -> Int -> [Int]
      hiddenLOF x 1 = [1]
      hiddenLOF x y = if x `mod` y == 0
         then [y] ++ (hiddenLOF x $ y-1)
         else hiddenLOF x $ y-1

listOfPrimeFactors :: [Int] -> [Int]
listOfPrimeFactors [] = []
listOfPrimeFactors (x:xs) = if isPrime x
   then [x] ++ listOfPrimeFactors xs
   else listOfPrimeFactors xs

answer :: Int
answer = head . listOfPrimeFactors . listOfFactors $ 600851475143
