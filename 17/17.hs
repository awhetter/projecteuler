digitToString :: Char -> String
digitToString toParse =
	case toParse of
		'0' -> "zero"
		'1' -> "one"
		'2' -> "two"
		'3' -> "three"
		'4' -> "four"
		'5' -> "five"
		'6' -> "six"
		'7' -> "seven"
		'8' -> "eight"
		'9' -> "nine"

tensToString :: Char -> String
tensToString toParse =
	case toParse of
		'2' -> "twenty"
		'3' -> "thirty"
		'4' -> "forty" {-"fourty"-}
		'5' -> "fifty"
		'6' -> "sixty"
		'7' -> "seventy"
		'8' -> "eighty"
		'9' -> "ninety"
	
numToString :: Int -> String
numToString toParse = hiddenNTS 0 (reverse (show toParse)) ""
	where
		hiddenNTS :: Int -> String -> String -> String
		hiddenNTS _ "" toAddTo = toAddTo
		hiddenNTS powerOfTen (digit:rest) toAddTo =
			case powerOfTen of
				0 -> hiddenNTS (powerOfTen + 1) rest $ digitToString digit
				1 -> if (digit /= '1') && (digit /= '0') then
						if toAddTo == "zero" then
							hiddenNTS (powerOfTen + 1) rest $ tensToString digit
						else
							hiddenNTS (powerOfTen + 1) rest $ tensToString digit {-++ " "-} ++ toAddTo
					else if digit == '1' then
						---Deal with 1x---
						case toAddTo of
							"zero" -> hiddenNTS (powerOfTen + 1) rest $ "ten"
							"one" -> hiddenNTS (powerOfTen + 1) rest $ "eleven"
							"two" -> hiddenNTS (powerOfTen + 1) rest $ "twelve"
							"three" -> hiddenNTS (powerOfTen + 1) rest $ "thirteen"
							"five" -> hiddenNTS (powerOfTen + 1) rest $ "fifteen"
							"eight" -> hiddenNTS (powerOfTen + 1) rest $ "eighteen"
							_ -> hiddenNTS (powerOfTen + 1) rest $ toAddTo ++ "teen"
					else
						---Assume first digit was a 0---
						hiddenNTS (powerOfTen + 1) rest toAddTo
				2 -> if digit == '0' then
						hiddenNTS (powerOfTen + 1) rest toAddTo
					else
						if toAddTo == "zero" then
							hiddenNTS (powerOfTen + 1) rest $ digitToString digit ++ "hundred" {-" hundred"-}
						else 
							hiddenNTS (powerOfTen + 1) rest $ digitToString digit ++ "hundredand" {-" hundred and "-} ++ toAddTo
				3 -> if toAddTo == "zero" then
						"onethousand" {-"one thousand"-}
					else
						"onethousand" {-"one thousand "-} ++ toAddTo
				_ -> toAddTo
				
answer :: Int
answer = sum [length (numToString x) | x <- [1..1000]]