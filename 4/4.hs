isPalindrome :: (Show a) => a -> Bool
isPalindrome toCheck = (show toCheck) == (reverse $ show toCheck)

answer :: Int
answer = maximum [(x*y) | x <- [999,998..100], y <- [999,998..100], (isPalindrome (x*y))]