isTriangle :: (Integral a) => a -> Bool
isTriangle toCheck = (snd.properFraction) (0.5*(sqrt(8*(fromIntegral toCheck)+1)-1)) == 0

isPentagonal :: (Integral a) => a -> Bool
isPentagonal toCheck = (snd.properFraction) ((1/6) * (1 + (sqrt (24 * (fromIntegral toCheck) + 1)))) == 0

isHexagonal :: (Integral a) => a -> Bool
isHexagonal toCheck = (snd.properFraction) (0.25 * (1 + (sqrt (8 * (fromIntegral toCheck) + 1)))) == 0

answer :: Integer
answer = head [x | x <- [(n * (n+1)) `div` 2 | n <- [286..]], (isPentagonal x), (isHexagonal x)]