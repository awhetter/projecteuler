filterMulOfFives :: [Int] -> [Int]
filterMulOfFives [] = []
filterMulOfFives (x:xs) = if (x `mod` 3 /= 0)
   then [x] ++ filterMulOfFives xs
   else filterMulOfFives xs

answer :: Int
answer = (sum [ x*3 | x <- [1..333]]) + (sum $ filterMulOfFives $ [ x*5 | x <- [1..199]])
