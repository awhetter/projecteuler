sumOfSquares :: Int
sumOfSquares = sum [x ^ 2 | x <- [1..100]]

squareOfSums :: Int
squareOfSums = (sum [1..100]) ^ 2

answer :: Int
answer = squareOfSums - sumOfSquares